# SharedAssembly

## What is this repository for?

[Shared Assembly](https://docs.microsoft.com/en-us/archive/blogs/jjameson/shared-assembly-info-in-visual-studio-projects) Info in Visual Studio Projects

## Author

Implemented by [Gopinath Rajendran](https://www.linkedin.com/in/gopinath-rajendran-2073ab55/)

## To clone the repository

 git clone --single-branch --branch master https://gitlab.com/gopinathrbe/sharedassemblyinfo.git